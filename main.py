from datetime import datetime, timedelta

import job

try:
    import googleclouddebugger

    googleclouddebugger.enable()
except ImportError:
    pass

from flask import Flask, render_template, request
from flask_caching import Cache

cache = Cache(config={'CACHE_TYPE': 'simple'})
app = Flask(__name__)
cache.init_app(app)


@app.route('/', methods=['GET'])
def get_data():
    return render_template("index.html", title="Italian Covidiots")


@app.route('/tasks/update', methods=['GET'])
def update():
    if request.headers['X-Appengine-Cron']:
        yesterday = datetime.now() - timedelta(days=1)
        job.run(yesterday)
    return "OK", 200


if __name__ == '__main__':
    app.run()
