import csv
import re
from io import BytesIO, StringIO

from datetime import datetime, timedelta
from google.cloud import storage

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage

import requests

DENOUNCED_SRC_URL = "https://www.interno.gov.it/sites/default/files/modulistica" \
                    "/monitoraggio_serviz_controllo_giornaliero_{}.pdf"
POSITIVE_SRC_URL = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-andamento-nazionale/dpc-covid19" \
                   "-ita-andamento-nazionale-{}.csv"

str_to_remove = [
    "Monitoraggio dei servizi di controllo",
    "D.P.C.M. 8 e 9 marzo 2020, recante misure urgenti per il",
    "contenimento della diffusione del virus COVID-19",
    "PERSONE CONTROLLATE",
    "PERSONE DENUNCIATE EX ART. 650 C.P.",
    "(In ottemperanza ai D.C.P.M. 8 e 9 marzo 2020)",
    "PERSONE DENUNCIATE EX ART. 495 E 496 C.P.",
    "(Falsa attestazione o dichiarazione a P.U. / False dichiarazioni",
    "sulla identità o su qualità personali proprie o di altri)",
    "ESERCIZI COMMERCIALI CONTROLLATI",
    "TITOLARI ESERCIZI COMMERCIALI DENUNCIATI EX",
    "ART. 650 C.P.",
    "SOSPENSIONE ESERCIZI COMMERCIALI EX ART. 15",
    "D.L. 14/2020"
]

r_to_remove = [
    r'Controlli effettuati nella giornata del [0-9]+ [a-z]+',
    r'[0-9]+ [a-z]+ 2020'
]


def get_new_positive(date):
    date_str = date.strftime("%Y%m%d")
    content_pc = requests.get(POSITIVE_SRC_URL.format(date_str))
    csv_content_pc = csv.DictReader(content_pc.content)
    data_pc = next(csv_content_pc)
    new_positive_today = data_pc['nuovi_attualmente_positivi']

    return new_positive_today


def get_new_denounced(date):
    date_str = date.strftime("%#d.%#m.%Y")  # 20.3.2020
    res_str = pdf_from_url_to_txt(DENOUNCED_SRC_URL.format(date_str))
    res_str = remove_noise(res_str)
    res_str = res_str.replace("\n", " ")
    res_str = res_str.strip()
    data = res_str.split(" ")
    data = [d for d in data if d != ""]

    return int(data[1].replace(".", "")) + int(data[2].replace(".", ""))


def remove_noise(str_to_clean):
    for s in str_to_remove:
        str_to_clean = str_to_clean.replace(s, "")

    for r in r_to_remove:
        str_to_clean = re.sub(r, "", str_to_clean)

    return str_to_clean


def pdf_from_url_to_txt(url):
    res_str = ""
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/80.0.3987.149 Safari/537.36"
    }

    r = requests.get(url, headers=headers)

    fh = BytesIO(r.content)
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos = set()
    for page in PDFPage.get_pages(fh, pagenos, maxpages=maxpages, password=password,
                                  caching=caching, check_extractable=True):
        interpreter.process_page(page)
        device.close()
        res_str = retstr.getvalue()
        retstr.close()
        fh.close()

    return res_str


def write_data(date, new_denounced, new_positive):
    storage_client = storage.Client()
    bucket = storage_client.bucket("italian-covidiots")
    blob = bucket.blob("data.csv")

    new_date = date.strftime("%Y-$m-%d")
    csv_string = blob.download_as_string()
    if new_date not in csv_string:
        csv_string = csv_string + "\n" + new_date + "," + new_denounced + "," + new_positive

        blob.upload_from_string(csv_string)


def run(date):
    new_positive = get_new_positive(date)
    new_denounced = get_new_denounced(date)
    write_data(date, new_denounced, new_positive)


if __name__ == '__main__':
    yesterday = datetime.now() - timedelta(days=1)
    run(yesterday)
