from datetime import datetime, timedelta
from unittest import TestCase

import job


class Test_Job(TestCase):

    def test_get_new_denounced(self):
        date = datetime.strptime("2020-03-20", "%Y-%m-%d")
        today = job.get_new_denounced(date)
        assert today != 0
